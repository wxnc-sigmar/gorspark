package com.nextcode.gor.platform;

public interface GorLogSubscription extends AutoCloseable{
    void start();
}
